// This file is part of Volatile.
//
// Volatile is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Volatile is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Volatile. If not, see <https://www.gnu.org/licenses/>.

use std::sync::Arc;

use actix_files;
use actix_web::{web, App, HttpServer, Responder};
use anyhow::Result;

mod db;
use db::Work;

const STATIC_DIR: &str = "src/static";

#[actix_web::get("/api/get-work")]
async fn get_work(db: web::Data<Arc<db::Conn>>) -> impl Responder {
    web::Json(db.get_work().unwrap())
}

#[actix_web::post("/api/submit-work")]
async fn submit_work(request: web::Json<Work>, db: web::Data<Arc<db::Conn>>) -> impl Responder {
    let work = request.into_inner();
    println!("{:?}", work);
    db.submit_work(work).unwrap();

    ""
}

#[actix_web::main]
async fn main() -> Result<()> {
    let db = Arc::new(db::Conn::new()?);

    HttpServer::new(move || {
        App::new()
            .service(get_work)
            .service(submit_work)
            .service(actix_files::Files::new("/", STATIC_DIR).index_file("index.html"))
            .app_data(web::Data::new(db.clone()))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await?;
    Ok(())
}
