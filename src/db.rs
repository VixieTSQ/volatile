// This file is part of Volatile.
//
// Volatile is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Volatile is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Volatile. If not, see <https://www.gnu.org/licenses/>.

use std::sync::{LockResult, Mutex, Once};

use anyhow::Result;
use rusqlite::{params, Connection};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
enum Emotion {
    Happy,
    Angry,
    Surprise,
    Sad,
    Fear,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Work {
    word: String,
    emotion: Option<Emotion>,
    polarity: Option<bool>,
}

/// If a connection to the database already exists then this must be true.
static DB_EXISTS: Once = Once::new();

/// Path to the database file.
const DB_PATH: &str = "db.sqlite";

/// A string containing the SQL statements to setup the database.
const INIT_SQL: &str = include_str!("init.sql");

/// Struct containing a rusqlite connection and abstract methods to interact
/// with it.
pub struct Conn {
    pub connection: Mutex<rusqlite::Connection>,
}

impl Conn {
    /// Get database connection. Will create database if it doesn't exist.
    /// Deletes database first if .env DEV tag is set to true.
    pub fn new() -> Result<Conn> {
        let mut result = None;
        DB_EXISTS.call_once(|| {
            result = Some(Self::db_init_internal());
        });
        result.unwrap()
    }

    /// Used internally by Conn::new().
    fn db_init_internal() -> Result<Conn> {
        let conn;
        let path = DB_PATH;
        conn = Connection::open(&path)?;
        conn.execute_batch(INIT_SQL)?;
        Ok(Conn {
            connection: Mutex::new(conn),
        })
    }
    pub fn get_work(&self) -> Result<String> {
        let conn = self.connection.lock().ignore();
        let work_value: String = conn.query_row(
            "SELECT prompt
        FROM prompts
        LIMIT 1
        OFFSET ABS(RANDOM()) % MAX((SELECT COUNT(*) FROM prompts), 1)",
            [],
            |row| row.get(0),
        )?;
        Ok(work_value)
    }
    pub fn submit_work(&self, work: Work) -> Result<()> {
        let emotion: Option<&str> = work.emotion.and_then(|v| match v {
            Emotion::Happy => Some("Happy"),
            Emotion::Angry => Some("Angry"),
            Emotion::Surprise => Some("Surprise"),
            Emotion::Sad => Some("Sad"),
            Emotion::Fear => Some("Fear"),
        });
        let polarity = work.polarity.map(|v| if v { 1 } else { 0 });
        let conn = self.connection.lock().ignore();
        let mut stmt =
            conn.prepare("INSERT INTO works (prompt, emotion, polarity) VALUES (?, ?, ?)")?;
        match (emotion, polarity) {
            (None, None) => stmt.insert(params![
                &work.word,
                rusqlite::types::Null,
                rusqlite::types::Null
            ])?,
            (None, Some(polarity)) => {
                stmt.insert(params![&work.word, rusqlite::types::Null, polarity])?
            }
            (Some(emotion), None) => {
                stmt.insert(params![&work.word, emotion, rusqlite::types::Null])?
            }
            (Some(emotion), Some(polarity)) => {
                stmt.insert(params![&work.word, emotion, polarity])?
            }
        };
        Ok(())
    }
}

pub trait LockResultExt {
    type Guard;

    fn ignore(self) -> Self::Guard;
}

impl<Guard> LockResultExt for LockResult<Guard> {
    type Guard = Guard;

    fn ignore(self) -> Guard {
        self.unwrap_or_else(|e| e.into_inner())
    }
}
