// This file is part of Volatile.
//
// Volatile is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Volatile is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Volatile. If not, see <https://www.gnu.org/licenses/>.

const prompt = document.getElementById("prompt"); 
const next = document.getElementById("next");
const emotions = [
    document.getElementById("happy"),
    document.getElementById("angry"),
    document.getElementById("surprise"),
    document.getElementById("sad"),
    document.getElementById("fear"),
    document.getElementById("none")
]
const polarities = [
    document.getElementById("positive"),
    document.getElementById("negative"),
    document.getElementById("none-polarity")
]
var currentEmotion = null;
var currentPolarity = null;

const resizePromptFont = () => prompt.style.fontSize = prompt.offsetWidth / 6 + "px";
const getWork = () => fetch("/api/get-work").then(r => r.json()).then(work => {
    prompt.innerText = work;
});

resizePromptFont();
getWork();

emotions.forEach(emotion => emotion.addEventListener("click", () => {
    emotions.forEach(emotion => emotion.classList.remove("selected"));
    emotion.classList.add("selected");
    if ( emotion == emotions[5] ) {currentEmotion = null}
    else {currentEmotion = emotion.innerText};
}));
polarities.forEach(polarity => polarity.addEventListener("click", () => {
    polarities.forEach(polarity => polarity.classList.remove("selected"));
    polarity.classList.add("selected");
    if ( polarity == polarities[0] ) { currentPolarity = true }
    if ( polarity == polarities[1] ) { currentPolarity = false }
    if ( polarity == polarities[2] ) { currentPolarity = null }
}));

window.addEventListener('resize', function(event){
    resizePromptFont();
});
next.addEventListener("click", () => {
    fetch("/api/submit-work", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "word": prompt.innerText,
            "emotion": currentEmotion,
            "polarity": currentPolarity
        })
    });

    polarities.forEach(polarity => polarity.classList.remove("selected"));
    polarities[2].classList.add("selected");
    emotions.forEach(emotion => emotion.classList.remove("selected"));
    emotions[5].classList.add("selected");
    currentPolarity = null;
    currentEmotion = null;

    getWork();
});